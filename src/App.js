import React from 'react';
import './App.css';
import 'bootstrap/dist/css/bootstrap.min.css';
import TaskComponent from './components/TaskComponent';

function App() {
  return (
    <div className="container">
      <TaskComponent />
    </div>
  );
}

export default App;
