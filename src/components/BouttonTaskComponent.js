import React from 'react';

function BouttonTaskComponent (props) {

    return (
        <div>
            <button className={props.styles} onClick={props.funct}>{props.name}</button>
        </div>
    );
}

export default BouttonTaskComponent