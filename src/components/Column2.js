import React from 'react';

function Column2({left,right}){

    return(
        <div className="row">
            <div className="col-sm-12 col-md-6 col-lg-6">{left}</div>
            <div className="col-sm-12 col-md-6 col-lg-6">{right}</div>
        </div>
    )
}

export default Column2