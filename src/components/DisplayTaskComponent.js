import React from 'react';

function DisplayTaskComponent (props){

   
        return (
            <div>
                {
                        
                    <table>
                        <thead>
                            <tr>
                                <th colSpan="2">Tasks</th>
                                <th colSpan="2">Description</th>
                            </tr>
                        </thead>
                        {
                            props.task.map((tasks) =>(
                                <tbody>
                                <tr>
                                    <td colSpan="2">{tasks.title}</td>
                                    <td colSpan="2">{tasks.description}</td>
                                    <td  key={tasks.id}>
                                        {tasks.complete}
                                        <button className="btn btn-warning"
                                            onClick={ ()=> props.onTaskStatus(tasks)}>
                                            {tasks.complete && "✅ completed"}
                                            {!tasks.complete && "⬜ uncompleted"}
                                        </button>
                                    </td>
                                    <td>
                                        <button className="btn btn-success" 
                                        onClick={() => props.updateTask(tasks.id)}> Modifier
                                        </button>
                                    </td>
                                    <td>
                                        <button  className="btn btn-danger" 
                                        onClick={() =>props.deleteTask(tasks.id)}> Suprimer
                                        </button>
                                    </td>
                                </tr>
                            </tbody>
                            ))
                        }
                            
                    </table>
                }
            </div>
        )
}
export default DisplayTaskComponent