import React from 'react';

function FormTask (props) {

    return (
        <div>
            <form className="mt-4" onSubmit={props.handleSaveTask}>
                <div className="form-group">
                    <label>Add Task</label>
                    <input text="text" className="form-control" name="title" value={props.title} 
                        placeholder="entrer votre taches"
                        onChange={props.onInputChange}/>
                </div>
                <div className="form-group  mb-2">
                    <label>Task Description</label>
                    <textarea text="text" className="form-control" value={props.description} placeholder="description de votre taches"
                    onChange={props.onInputChange} name="description">

                    </textarea>
                </div>
                <div className="form-group">
                    <input className="btn btn-success " mt-4 type="submit" value="Add Task"/>
                </div>
            </form>
        </div>
    );
}

export default FormTask