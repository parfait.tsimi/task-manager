import React from 'react';

function ModifierTaskComponent (props) {

    let newTitle='';
    let newDescription='';
    function handleUpdateTask(e){
        e.preventDefault();
        props.editTaskFromState(props.task[0].id, newTitle.value,newDescription.value);
        props.handleCancel();
    }
    return (
        <div>
            <form className="mt-4" onSubmit={handleUpdateTask}>
                <div className="form-group">
                    <label>Add Task</label>
                    <input text="text" className="form-control" name="title" defaultValue={props.task[0].title} 
                        ref={(node) =>{
                            newTitle=node
                        }}/>
                </div>
                <div className="form-group  mb-2">
                    <label>Task Description</label>
                    <textarea text="text" className="form-control" defaultValue={props.task[0].description} 
                    ref={(node) =>{newDescription=node}} name="description">
                    </textarea>
                </div>
                <div className="form-group mt-4">
                    <input className="btn btn-success"  type="submit" value="Update Task"/>
                    <button className="btn btn-warning"  type="button" 
                     onClick={props.handleCancel}>Cancel</button>
                </div>
            </form>
        </div>
    );
}

export default ModifierTaskComponent