import React, {useState} from 'react';

function SearchTaskComponent (props) {

    const [task, setFormData] =useState({
        title:''
    })

    const handleSearchTask = e =>{
        e.preventDefault();
        props.searchTask(task.title);
    }
   

    return (
        <div className="row">
            <form onSubmit={handleSearchTask} style={{display:"flex",marginTop:"20px"}}>
                <div className="form-group" style={{marginLeft:"150px"}}>
                    <label>Search Task</label>
                    <input text="text" style={{width:"250px"}} className="form-control" value={task.title} 
                        placeholder="rechercher votre tache ici"
                        onChange={(e) => setFormData({...task, title: e.target.value})}/>
                </div>
                <input className="btn btn-success mt-4" type="submit" value="Search Task"/>
            </form>
        </div>
    );
}

export default SearchTaskComponent