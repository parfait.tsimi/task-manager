import React, {Component} from 'react';
import FormTask from './FormTask';
//import SearchTaskComponent from './SearchTaskComponent';
import BouttonTaskComponent from './BouttonTaskComponent';
import Column2 from './Column2';
import DisplayTaskComponent from './DisplayTaskComponent';
import { v4 as uuidv4 } from 'uuid';
import ModifierTaskComponent from './ModifierTaskComponent';

class TaskComponent extends Component{

    state={
        currentTask:{
            id:'',
            title:'',
            description:'',
            complete:false
        },
        edit:false,
        show:false,
        task:[]
    }
    

    handleShowAllTask = () =>{
        const tasks=JSON.parse(localStorage.getItem('task'));
        this.setState({
            task:tasks
         })
    }

    /*handleShowCompletedTask = () =>{
        const tasks=JSON.parse(localStorage.getItem('task'));
        tasks.map((tasklist) =>{
            if (tasklist.complete ===true) {
                tasklist.complete=true;
            }
            console.log(tasklist)
            return tasklist
        });
        this.setState({
            task:tasks
        })
        console.log(tasks)
    }

    handleShowUncompletedTask = () =>{
        const tasks=[JSON.parse(localStorage.getItem('task'))];
        tasks.filter(item =>item.complete ===false);
        this.setState({
            task:tasks
        })
        console.log(this.state.task)
    }*/

    handleSaveTask = e =>{
        e.preventDefault();
        let newTask =this.state.currentTask;
        if(newTask.title!==""){
            const newTasks=[...this.state.task,newTask];
            this.setState({
                task:newTasks,
                currentTask:{
                    id:'',
                    title:'',
                    description:'',
                    complete:false
                }
            })
            localStorage.setItem('task',JSON.stringify(newTasks));
        }
    }
    
    editTaskFromState = (id,newTitle,newDescription) =>{
        
        const newTask=this.state.task.map((taskEdit) =>{
            if (id ===taskEdit.id) {
                
                taskEdit.title=newTitle;
                taskEdit.description=newDescription;
            }
            return taskEdit
        });
        this.setState({
            task:newTask,
        });
    };

    onInputChange= (input) =>{
        const{name, value} = input.target;
        switch(name){
            case 'title':
                this.setState({
                    currentTask:{title:value,description:''}
                });
            break;
            case 'description':
                this.setState({
                    currentTask:{title:this.state.currentTask.title, description:value}
                });
            break;
            default:
        }
        this.setState((state) =>({
            currentTask:{
                id:uuidv4(),
                title:state.currentTask.title,
                description:state.currentTask.description,
                complete:false
            },
        }));
        
    }

    deleteTask=id =>{
        /*e.preventDefault();
        const array = this.state.task;
        const index = array.indexOf(e.target.value);
        array.splice(index,1);
            this.setState({
                task:array
            });
        */
        //function deleteTodo(id){
            const deleteTask = [...this.state.task].filter(
                (item) =>item.id !==id);
            this.setState({
                task:deleteTask
            })
       // }
    }

    updateTask = (id) =>{
        const taskUpdate = [...this.state.task];
        const taskFound=taskUpdate.filter((item) =>item.id===id);
        this.setState({
            edit:true,
            task:taskFound
        });
    }
    handleCancel = () =>{
        this.setState({
            edit:false,
            task:this.state.task
        }); 
    }
   
    onTaskStatus = (tasks) => {
        let taskChek=this.state.task.map((checkTask) => {
            checkTask.complete =
              tasks.id === checkTask.id ? !checkTask.complete : checkTask.complete;
                return checkTask;
             }
          );
        this.setState({
            task:taskChek
        });
    }
    render(){
        if(!this.state.edit){
            return(
                <div className="container">
                    <h1>SIMPLE TASK MANAGER APPLICATION</h1>
                   <Column2 left={<FormTask 
                   title={this.state.currentTask.title} description={this.state.currentTask.description} 
                   handleSaveTask={this.handleSaveTask} onInputChange={this.onInputChange}/>}
                           /* right={<SearchTaskComponent searchTask={this.searchTask}/>}*/
                    />
                    <div className="row">
                        <div style={{display:"flex",marginTop:"20px"}}>
                            <div style={{marginRight:"10px"}}>
                                <BouttonTaskComponent name="Show all task" 
                                    funct={this.handleShowAllTask} styles="btn btn-success"/>
                            </div>
                            <div  style={{marginLeft:"10px",marginRight:"10px"}}>
                                <BouttonTaskComponent name="Show task completed" 
                                    funct={this.handleShowCompletedTask} styles="btn btn-success"/> 
                            </div>
                            <div  style={{marginLeft:"10px",marginRight:"10px"}}>
                                <BouttonTaskComponent name="Show task uncompleted" 
                                    funct={this.handleShowUncompletedTask} styles="btn btn-success"/>
                            </div>
                        </div>
                    </div>
                    {
                        (!this.state.show)?
                        (<div style={{display:'flex', marginTop:'20px'}}>
                            <DisplayTaskComponent task={this.state.task} 
                                title={this.state.currentTask.title}
                                description={this.state.currentTask.description}
                                updateTask={this.updateTask} 
                                deleteTask={this.deleteTask}
                                onTaskStatus={this.onTaskStatus}/>
                        </div>):(null)
                    }
                </div>
            );
        
        }else{
            return(
                <div className="container">
                   <Column2 left={<FormTask 
                   title={this.state.currentTask.title} description={this.state.currentTask.description} 
                   handleSaveTask={this.handleSaveTask} onInputChange={this.onInputChange}/>}
                    />
                    <ModifierTaskComponent handleCancel={this.handleCancel} task={this.state.task}
                    editTaskFromState={this.editTaskFromState} onInputChange={this.onInputChange}/>
                </div>
            );
        }
        
    }
}

export default TaskComponent;